﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace MedicalNeuralNetwork
{
    /// <summary>
    /// Логика взаимодействия для Description.xaml
    /// </summary>
    public partial class Description : Window
    {
        string window;
        public Description(string head, string backWindow)
        {
            DataBase db = new DataBase();
            string[] m_Info = db.GetInfo(head);

            InitializeComponent();

            DescrTextBox.Text = m_Info[0];//Присвоили значение описанию
            RecTextBox.Text = m_Info[1];//Присвоили значение рекомендациям
            HeaddTextBox.Text = head; // Присвоили заначение заголовку, которое получили
            window = backWindow; // Название окна из которого мы сюда пришли
        }

        // Переход в предыдущее окно
        private void Button_Click(object sender, RoutedEventArgs e)
        {
            switch (window)
            {
                case "MainWindow": // Если из главного
                    Application.Current.MainWindow.Show();
                    this.Close();
                    break;
                case "Catalog": // Если из каталога
                    Application.Current.Windows.OfType<Catalog>().First().Show(); // В коллекции окон проекта нашли окно класса Catalog
                    this.Close();
                    break;
            }
        }

        private void TextBox_TextChanged(object sender, TextChangedEventArgs e)
        {

        }

        private void ExitButton_Click(object sender, RoutedEventArgs e)
        {
            Environment.Exit(0); // В момент, когда закрылось главное окно, все приложение закрывается, не зависимо, от того, открыто ли еще что-нибудь
        }

        private void HomeButton_Click(object sender, RoutedEventArgs e)
        {
            Application.Current.MainWindow.Show();
            this.Close();
        }
    }
}
