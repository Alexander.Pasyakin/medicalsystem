﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Diagnostics;
using System.Net;
using System.Data.SqlClient;

namespace MedicalNeuralNetwork
{
    /// <summary>
    /// Логика взаимодействия для MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private Uri searchUri = new Uri("SearchPage.xaml", UriKind.Relative);//Uri адрес страницы поиска
        private Uri catalogUri = new Uri("CatalogPage.xaml", UriKind.Relative);//Uri адрес страницы каталога
        private Uri descriptionUri = new Uri("DescriptionPage.xaml", UriKind.Relative);//Uri адрес страницы описания
        private Uri mainUri = new Uri("MainPage.xaml", UriKind.Relative);//Uri адрес главной страницы
        private Uri settingsUri = new Uri("SettingsPage.xaml", UriKind.Relative);//Uri адрес страницы настроек
        private Uri infoUri = new Uri("InfoPage.xaml", UriKind.Relative);//Uri адрес страницы информации

        public MainWindow()
        {
            InitializeComponent();

            MainFrame.NavigationService.Navigate(mainUri);//Отображение главной страницы
        }

        private void Head_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            this.DragMove();// Для перемещения ока
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            checkErrors(null);//Проверяем состояние итернета и доступа к БД

            if (MainFrame.NavigationService.CanGoBack)//Если есть страницы в истории
                MainFrame.NavigationService.GoBack();//Переход к предыдущей странице
        }

        private void SearchWindow_Closed(object sender, EventArgs e)
        {
            Environment.Exit(0); // В момент, когда закрылось главное окно, все приложение закрывается, не зависимо, от того, открыто ли еще что-нибудь
        }

        private void HomeButton_Click(object sender, RoutedEventArgs e)
        {
            MainFrame.NavigationService.Navigate(mainUri);//Переход на главную страницу
        }

        private void SettingsButton_Click(object sender, RoutedEventArgs e)
        {
            MainFrame.NavigationService.Navigate(settingsUri);//Переход на страницу настроек
        }

        private void CloseButton_Click(object sender, RoutedEventArgs e)//При нажатии на кнопку "Закрыть окно"
        {
            ExitWondow ew = new ExitWondow();//Создаем экземпляр окна выхода
            ew.Show();//Показать окно выхода
        }

        private void FullscreenButton_Click(object sender, RoutedEventArgs e)//Переход в полноэкранный режим
        {
            if(WindowMain.WindowState == WindowState.Normal)//Если окно не в полноэкранном режиме
            {
                WindowState = WindowState.Maximized;//Развернуть окно на полный экран
                FullscreenImage.Source = new BitmapImage(new Uri("Icons/window.ico", UriKind.Relative));//Поменять картинку кнопки "На полный экран"
                return;
            }
            if (WindowMain.WindowState == WindowState.Maximized)//Если окно в полноэкранном режиме
            {
                WindowState = WindowState.Normal;//Свернуть окно в нормальное состояние
                FullscreenImage.Source = new BitmapImage(new Uri("Icons/fullscreen.ico", UriKind.Relative));//Поменять картинку кнопки "На полный экран"
                return;
            }
        }

        private void HideButton_Click(object sender, RoutedEventArgs e)//При нажатии на кнопку "Свернуть окно"
        {
            WindowState = WindowState.Minimized;//Сворачиваем окно
        }

        private void CatalogButton_Click(object sender, RoutedEventArgs e)//Переход на страницу каталога
        {
            checkErrors(catalogUri);
        }

        private void ForwardButton_Click(object sender, RoutedEventArgs e)//Переход на следующую страницу в истории просмотра
        {
            checkErrors(null);//Проверка на ошибки

            if (MainFrame.NavigationService.CanGoForward)//Если следующая страница есть
                MainFrame.NavigationService.GoForward();//Переход на следующую страницу
        }

        private void RefreshButton_Click(object sender, RoutedEventArgs e)//Обновить страницу
        {
            MainFrame.NavigationService.Refresh();//Обноаляем страницу
        }

        private void DiagnosticButton_Click(object sender, RoutedEventArgs e)//Переход на страницу диагностики
        {
            checkErrors(searchUri);
        }

        //_________Проверка на ошибки_________//
        //На вход принимает Uri адресс страницы на которую хотим перейти
        //Если не знаем адрес страницы, то передаем NULL
        public void checkErrors(Uri uriPage)
        {
            DataBase db = new DataBase();

            //return db.ConnectionBase().ToString();

            if (db.ConnectionBase() != "OK")//Если имя ошибки не "ОК"
            {
                ErrorWindow ew = new ErrorWindow(db.ConnectionBase());//Инициализируем окно ошибки
                ew.Show();//Показываем окно ошибки
            }
            else//Иначе
            {
                if(uriPage != null)//Если Uri страницы не NULL
                    MainFrame.NavigationService.Navigate(uriPage);//Преходим на нужную страницу
            }
        }

        private void InfoButton_Click(object sender, RoutedEventArgs e)//Переход на страницу информации
        {
            MainFrame.NavigationService.Navigate(infoUri);//Переходим на страницу информации
        }
    }
}
