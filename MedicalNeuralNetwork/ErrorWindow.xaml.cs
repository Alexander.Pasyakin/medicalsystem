﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace MedicalNeuralNetwork
{
    /// <summary>
    /// Логика взаимодействия для ErrorWindow.xaml
    /// </summary>
    public partial class ErrorWindow : Window
    {
        BitmapImage bmi = new BitmapImage();

        public ErrorWindow(string errorName)
        {
            InitializeComponent();

            errorTextBlock.Text = errorName;
            //errors(errorName);//Вызываем метод вывода ошибки
        }

        private void errors(string nameError)//Отрисовываем окно в зависимости от ошибки
        {
            bmi.BeginInit();

            string errorText = "";//Текст ошибки

            switch (nameError)//Выбираем что отрисовывать в окне
            {
                case "disconnectInternet"://Если нет соединения с интернетом                    
                    bmi.UriSource = new Uri("Icons/disconnectNet.jpg", UriKind.Relative);//Отрисовываем картинку отсутствия интернета
                    errorText = "Нет интернет-соединения. Проверьте подключение и повторите попытку.";//Пишем текст ошибки
                    this.Title = "Нет подключения к интернету";//Меняем имя окна ошибки
                    break;

                case "disconnectDataBase"://Если невозможно подключиться к базе данных
                    bmi.UriSource = new Uri("Icons/disconnectDataBase.jpg", UriKind.Relative);//Отрисовываем картинку отсутствия соединения с базой данных
                    errorText = "Не удается установить соединение с базой данных. Попробуйте выполнить действие позже.";//Пишем текст ошибки
                    this.Title = "Нет соединения с базой данных";//Меняем имя окна ошибки
                    break;
            }

            bmi.EndInit();
            errorImage.Stretch = Stretch.Fill;
            errorImage.Source = bmi;//Отрисовываем картинку ошибки
            errorTextBlock.Text = errorText;//Выводим текст ошибки
        }

        private void OKButton_Click(object sender, RoutedEventArgs e)//При нажатии на кнопку ОК
        {
            this.Close();//Закрываем окно ошибки
        }
    }
}
