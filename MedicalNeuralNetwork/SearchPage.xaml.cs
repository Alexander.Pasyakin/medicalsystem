﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace MedicalNeuralNetwork
{
    /// <summary>
    /// Логика взаимодействия для SearchPage.xaml
    /// </summary>
    public partial class SearchPage : Page
    {
        private Uri descriptionUri = new Uri("DescriptionPage.xaml", UriKind.Relative);
        public SearchPage()
        {
            InitializeComponent();

            // Здесь типо мы вытащили из БД весь список болезней (названия)
            for (int i = 0; i < 10; i++)
            {
                Label lb = new Label();
                ResultsList.Items.Add(lb.Content = "Болезнь " + i);
            }
        }

        private void ResultsList_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            

            //MessageBox.Show(ResultsList.SelectedValue.ToString());
        }

        private void MyButton_Click(object sender, RoutedEventArgs e)
        {
            ResultCheckText.Text = "";
            Object item;
            for (int i = 0; i < MyListBox.Items.Count; i++) // Цикл по листбоксу
            {
                item = MyListBox.Items[i]; // Присваеваем объекту элемент листбокса

                if (item is CheckBox) // Есди элемент листбокса является объектом класса CheckBox
                {
                    CheckBox c = (CheckBox)MyListBox.Items[i]; // Создаем объект класса CheckBox
                    ResultCheckText.Text += c.IsChecked.ToString() + "\n"; // Выводим в лейбл значение элемента
                }
            }
        }

        private void ResultsList_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            if (ResultsList.SelectedItem != null)
            {
                Data.nameDisease = ResultsList.SelectedItem.ToString();

                NavigationService.Navigate(descriptionUri);
            }
        }
    }
}
