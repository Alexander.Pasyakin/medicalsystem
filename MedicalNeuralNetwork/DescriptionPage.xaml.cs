﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace MedicalNeuralNetwork
{
    /// <summary>
    /// Логика взаимодействия для DescriptionPage.xaml
    /// </summary>
    public partial class DescriptionPage : Page
    {
        public DescriptionPage()
        {
            InitializeComponent();

            //Вытаскиваем из статического класса имя описание и рекомендации//
            NameLabel.Content = Data.nameDisease;

            descriptionText.Text = Data.description;

            recomendationText.Text = Data.recomendation;
        }
    }
}
