﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace MedicalNeuralNetwork
{
    /// <summary>
    /// Логика взаимодействия для CatalogPage.xaml
    /// </summary>
    public partial class CatalogPage : Page
    {
        private Uri descriptionUri = new Uri("DescriptionPage.xaml", UriKind.Relative); //Uri адрес страницы описания

        public CatalogPage()
        {
            InitializeComponent();

            // Здесь типо мы вытащили из БД весь список болезней (названия)
            for (int i = 0; i < 10; i++)
            {
                Label lb = new Label();
                DesiasesList.Items.Add(lb.Content = "Болезнь " + i);
            }
        }

        private void ResultsList_MouseDoubleClick(object sender, MouseButtonEventArgs e)//Переход на страницу описания по двойному клику
        {
            if (DesiasesList.SelectedItem != null)//Если элемент не пустой
            {
                Data.nameDisease = DesiasesList.SelectedItem.ToString();//Записываем в статический класс имя выбранного элемента

                Data.description = "";//Обнуляем в статическом классе описание

                Data.recomendation = "";//Обнуляем в статическом классе рекомендации

                //________Здесь вместо цикла должна быть запись описания и рекомендаций в статический класс_______//
                Random rn = new Random();

                for(int i = 0; i < rn.Next(1000, 10000); i++)
                {
                    Data.description += "Description " + i + " ";//Записываем описание
                }

                for(int i = 0; i < rn.Next(1000, 10000); i++)
                {
                    Data.recomendation +="Recomendation " + i + " ";//Записываем рекомендации
                }

                NavigationService.Navigate(descriptionUri);//Переход на страницу описания
            }
        }
    }
}
