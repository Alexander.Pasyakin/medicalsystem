﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace MedicalNeuralNetwork
{
    /// <summary>
    /// Логика взаимодействия для Catalog.xaml
    /// </summary>
    public partial class Catalog : Window
    {
        public Catalog()
        {
            InitializeComponent();

            DataBase db = new DataBase();
            //string[,] dm_all = db.GetAll();
            string[] m_Dis = db.GetDis();

            for (int i = 0; i < 75 + 1; i++)
            {
                Label lb = new Label();
                DesiasesList.Items.Add(lb.Content = m_Dis[i]);

            }


            // Здесь типо мы вытащили из БД весь список болезней (названия)
            //for (int i = 0; i < 10; i++)
            //{
            //    Label lb = new Label();
            //    DesiasesList.Items.Add(lb.Content = "Болезнь " + i);

            //}

        }

        // Переход на главное окно
        private void Button_Click(object sender, RoutedEventArgs e)
        {
            Application.Current.MainWindow.Show();
            this.Close(); // Полностью закрыли текущее окно
        }

        private void Window_Closed(object sender, EventArgs e)
        {

        }


        // Переход в окно Description по аналогии перехода из результатов поиска
        private void DesiasesList_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            Description dscpt = new Description(DesiasesList.SelectedItem.ToString(), "Catalog");
            dscpt.Show();
            this.Hide();
        }
    }
}

