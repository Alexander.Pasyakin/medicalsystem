﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MySql.Data.MySqlClient;
using System.IO;

namespace MedicalNeuralNetwork
{
    class DataBase
    {

        public string[] GetDis()
        {
            
            string[] data = new string[3];

            using (StreamReader reader = new StreamReader("..\\..\\data.cfg"))
            {
                data = reader.ReadLine().Split(' ');
            }


            string connStr = "server=localhost;port=" + data[1] + ";user=" + data[2] + ";database=DataBase;password=" + data[3];

            MySqlConnection conn = new MySqlConnection(connStr);
            conn.Open();


            int numb = Convert.ToInt32(data[0]);
            string[] m_Dis = new string[numb+1];


            //string[] m_Column = new string[3] { "Disease", "Description_and_Symptoms", "Recommendations" };

            //for (int j = 0; j < 3; j++)
            //{
                for (int i = 0; i < numb + 1; i++)
                {
                    string SqlIn = "SELECT Disease FROM disease WHERE Id=" + i;
                    MySqlCommand command = new MySqlCommand(SqlIn, conn);

                    m_Dis[i] = command.ExecuteScalar().ToString();

                }
            //}


            conn.Close();
            return m_Dis;

        }

        public string[] GetInfo(string DisName)
        {

            string[] data = new string[3];

            using (StreamReader reader = new StreamReader("..\\..\\data.cfg"))
            {
                data = reader.ReadLine().Split(' ');
            }


            string connStr = "server=localhost;port=" + data[1] + ";user=" + data[2] + ";database=DataBase;password=" + data[3];

            MySqlConnection conn = new MySqlConnection(connStr);
            conn.Open();




            string[] m_Column = new string[2] { "Description_and_Symptoms", "Recommendations" };
            string[] m_Info = new string[2];
            for (int i = 0; i < 2; i++)
            {
                string SqlIn = "SELECT "+ m_Column[i] +" FROM disease WHERE Disease=" + "'"+DisName+"'";
                MySqlCommand command = new MySqlCommand(SqlIn, conn);

                m_Info[i] = command.ExecuteScalar().ToString();

            }


            conn.Close();
            return m_Info;
        }

        public string ConnectionBase(MySqlConnection conn)
        {
            string[] data = new string[3];

            using (StreamReader reader = new StreamReader("..\\..\\data.cfg"))
            {
                data = reader.ReadLine().Split(' ');
            }

            string connStr = "server=localhost;port=" + data[1] + ";user=" + data[2] + ";database=DataBase;password=" + data[3];

            try
            {
                conn.Open();

                return "OK";
            }
            catch(Exception e)
            {
                return e.InnerException.HResult.ToString();
            }
            

            
        }
    }
}