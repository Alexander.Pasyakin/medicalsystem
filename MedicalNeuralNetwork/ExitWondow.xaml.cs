﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace MedicalNeuralNetwork
{
    /// <summary>
    /// Логика взаимодействия для ExitWondow.xaml
    /// </summary>
    public partial class ExitWondow : Window
    {
        public ExitWondow()
        {
            InitializeComponent();
        }

        private void OKButton_Click(object sender, RoutedEventArgs e)//При нажатии на кнопку ОК
        {
            Environment.Exit(0);//Закрываем все приложение
        }

        private void CancelButton_Click(object sender, RoutedEventArgs e)//При нажатии на кнопку отмены
        {
            this.Close();//Закрываем окно предупреждения и продолжаем работу
        }
    }
}
